module.exports = {
  bracketSpacing: true,
  printWidth: 100,
  tabWidth: 2,
  semi: false,
  singleQuote: true,
  jsxSingleQuote: false,
  useTabs: false,
  trailingComma: 'none',
  jsxBracketSameLine: true
}
