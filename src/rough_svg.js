import rough from 'roughjs/bundled/rough.esm.js'

// 通过 translate 变换将 Canvas 画布的坐标原点，从左上角 (0, 0) 点移动至 (256, 256) 位置，
// 即画布的底边上的中点位置。接着，以移动了原点后新的坐标为参照，通过 scale(1, -1) 将 y 轴向下的部分，
// 即 y>0 的部分沿 x 轴翻转 180 度，这样坐标系就变成以画布底边中点为原点，x 轴向右，y 轴向上的坐标系了。
export default function roughSVG() {
  const svg = document.getElementById('rough_svg')
  const rc = rough.svg(svg)

  svg.appendChild(
    rc.path('M400 100 h 90 v 90 h -90z', {
      stroke: 'red',
      strokeWidth: '3',
      fill: 'rgba(0,0,255,0.2)',
      fillStyle: 'solid'
    })
  )
  svg.appendChild(
    rc.path('M400 250 h 90 v 90 h -90z', {
      fill: 'rgba(0,0,255,0.6)',
      fillWeight: 4,
      hachureGap: 8
    })
  )
  svg.appendChild(
    rc.path('M37,17v15H14V17z M50,0H0v50h50z', {
      stroke: 'red',
      strokeWidth: '1',
      fill: 'blue'
    })
  )
  svg.appendChild(rc.path('M80 80 A 45 45, 0, 0, 0, 125 125 L 125 80 Z', { fill: 'green' }))
  svg.appendChild(
    rc.path('M230 80 A 45 45, 0, 1, 0, 275 125 L 275 80 Z', {
      fill: 'purple',
      hachureAngle: 60,
      hachureGap: 5
    })
  )
  svg.appendChild(rc.path('M80 230 A 45 45, 0, 0, 1, 125 275 L 125 230 Z', { fill: 'red' }))
  svg.appendChild(rc.path('M230 230 A 45 45, 0, 1, 1, 275 275 L 275 230 Z', { fill: 'blue' }))
}
