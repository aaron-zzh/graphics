import rough from 'roughjs/bundled/rough.esm.js'

// 通过 translate 变换将 Canvas 画布的坐标原点，从左上角 (0, 0) 点移动至 (256, 256) 位置，
// 即画布的底边上的中点位置。接着，以移动了原点后新的坐标为参照，通过 scale(1, -1) 将 y 轴向下的部分，
// 即 y>0 的部分沿 x 轴翻转 180 度，这样坐标系就变成以画布底边中点为原点，x 轴向右，y 轴向上的坐标系了。
export default function canvasTransform() {
  const rc = rough.canvas(document.querySelector('.canvas_t'))
  const ctx = rc.ctx
  ctx.translate(256, 256)
  ctx.scale(1, -1)

  const hillOpts = { roughness: 2.8, strokeWidth: 2, fill: 'blue' }

  rc.path('M-180 0L-80 100L20 0', hillOpts)
  rc.path('M-20 0L80 100L180 0', hillOpts)

  rc.circle(0, 150, 105, {
    stroke: 'red',
    strokeWidth: 4,
    fill: 'rgba(255,255, 0, 0.4)',
    fillStyle: 'solid'
  })
}
