import { multiply } from './lib/math/functions/Mat3Func.js'

// 转换css transform参数，优化性能
// transform: rotate(30deg) translate(100px,50px) scale(1.5);
// transform: matrix(1.3, 0.75, -0.75, 1.3, 61.6, 93.3);
export default function printTransformMatrix() {
  const rad = Math.PI / 6
  const a = [Math.cos(rad), -Math.sin(rad), 0, Math.sin(rad), Math.cos(rad), 0, 0, 0, 1]

  const b = [1, 0, 100, 0, 1, 50, 0, 0, 1]

  const c = [1.5, 0, 0, 0, 1.5, 0, 0, 0, 1]

  const res = [a, b, c].reduce((a, b) => {
    return multiply([], b, a)
  })

  console.log(res)
  /*
[1.299038105676658, -0.7499999999999999, 61.60254037844388, 
  0.7499999999999999, 1.299038105676658, 93.30127018922192,
  0, 0, 1]
*/
}
