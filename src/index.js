import _ from 'lodash'
import printMe from './print.js'
import webGLDraw from './webgl.js'
import canvasTransform from './transform.js'
import roughSVG from './rough_svg.js'
import drawTree from './vector_tree.js'
import drawPolygon from './vector_polygon.js'
import drawArc from './draw_arc.js'
import paramCurve from './parametric_curve.js'
import drawBezierCubic from './bezier_curve_cubic.js'
import drawBezierQuad from './bezier_curve_quadratic.js'
import fillPolygon from './polygon_fill'
import isPointInPath1 from './isPointInPath'
import isPointInPath2 from './isPointInPath2'
import webGLTriangulation from './triangulation'
import webGLParticles from './webgl_particles'
import printTransformMatrix from './css_transform_matrix'

import * as d3 from 'd3-hierarchy'
import './css/style.css'

const dataSource = 'https://s5.ssl.qhres.com/static/b0695e2dd30daa64.json'

;(async function () {
  // webpack 测试
  for (let i of [1, 2, 3]) {
    console.log(i)
  }
  function component() {
    var element = document.createElement('div')
    var btn = document.createElement('button')

    // Lodash, now imported by this script
    element.innerHTML = _.join(['Hello', 'webpack'], ' ')
    element.classList.add('hello')

    btn.innerHTML = '点击并查看控制台输出'
    btn.onclick = printMe
    element.appendChild(btn)

    return element
  }

  document.body.appendChild(component())

  // canvas
  function drawRect() {
    const canvas = document.querySelector('.canvas1')
    const context = canvas.getContext('2d')
    const rectSize = [100, 100]
    context.fillStyle = 'red'
    context.beginPath()

    //context.rect(0.5 * (canvas.width - rectSize[0]), 0.5 * (canvas.height - rectSize[1]), ...rectSize);
    context.save()
    context.translate(-0.5 * rectSize[0], -0.5 * rectSize[1])
    context.rect(0.5 * canvas.width, 0.5 * canvas.height, ...rectSize)
    context.fill()
    context.restore()
  }
  drawRect()

  //-------canvas-------
  const data = await (await fetch(dataSource)).json()
  const regions = d3
    .hierarchy(data)
    .sum((d) => 1)
    .sort((a, b) => b.value - a.value)

  const pack = d3.pack().size([1600, 1600]).padding(3)

  const root = pack(regions)

  const canvas = document.querySelector('.canvas2')
  const context = canvas.getContext('2d')
  const TAU = 2 * Math.PI

  function draw(ctx, node, { fillStyle = 'rgba(0, 0, 0, 0.2)', textColor = 'white' } = {}) {
    const children = node.children
    const { x, y, r } = node
    ctx.fillStyle = fillStyle
    ctx.beginPath()
    ctx.arc(x, y, r, 0, TAU)
    ctx.fill()
    if (children) {
      for (let i = 0; i < children.length; i++) {
        draw(context, children[i])
      }
    } else {
      const name = node.data.name
      ctx.fillStyle = textColor
      ctx.font = '1.5rem Arial'
      ctx.textAlign = 'center'
      ctx.fillText(name, x, y)
    }
  }

  draw(context, root)

  const svgroot = document.querySelector('.svg_hierarchy')

  function svgDraw(parent, node, { fillStyle = 'rgba(0, 0, 0, 0.2)', textColor = 'white' } = {}) {
    const children = node.children
    const { x, y, r } = node
    const circle = document.createElementNS('http://www.w3.org/2000/svg', 'circle')
    circle.setAttribute('cx', x)
    circle.setAttribute('cy', y)
    circle.setAttribute('r', r)
    circle.setAttribute('fill', fillStyle)
    circle.setAttribute('data-name', node.data.name)
    parent.appendChild(circle)
    if (children) {
      const group = document.createElementNS('http://www.w3.org/2000/svg', 'g')
      for (let i = 0; i < children.length; i++) {
        svgDraw(group, children[i], { fillStyle, textColor })
      }
      group.setAttribute('data-name', node.data.name)
      parent.appendChild(group)
    } else {
      const text = document.createElementNS('http://www.w3.org/2000/svg', 'text')
      text.setAttribute('fill', textColor)
      text.setAttribute('font-family', 'Arial')
      text.setAttribute('font-size', '1.5rem')
      text.setAttribute('text-anchor', 'middle')
      text.setAttribute('x', x)
      text.setAttribute('y', y)
      const name = node.data.name
      text.textContent = name
      parent.appendChild(text)
    }
  }

  svgDraw(svgroot, root)

  const titleEl = document.getElementById('title')

  function getTitle(target) {
    const name = target.getAttribute('data-name')
    if (target.parentNode && target.parentNode.nodeName === 'g') {
      const parentName = target.parentNode.getAttribute('data-name')
      return `${parentName}-${name}`
    }
    return name
  }

  let activeTarget = null
  svgroot.addEventListener('mousemove', (evt) => {
    let target = evt.target
    if (target.nodeName === 'text') target = target.previousSibling
    if (activeTarget !== target) {
      if (activeTarget) activeTarget.setAttribute('fill', 'rgba(0, 0, 0, 0.2)')
    }
    target.setAttribute('fill', 'rgba(0, 128, 0, 0.1)')
    titleEl.textContent = getTitle(target)
    activeTarget = target
  })
  // 使用 WebGL 绘制三角形
  webGLDraw()
  canvasTransform()
  roughSVG()
  drawTree()
  drawPolygon()
  drawArc()
  paramCurve()
  drawBezierCubic()
  drawBezierQuad()
  fillPolygon()
  isPointInPath1()
  isPointInPath2()
  webGLTriangulation()
  webGLParticles()
  printTransformMatrix()
})()
