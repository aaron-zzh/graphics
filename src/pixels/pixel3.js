import { loadImage, getImageData, traverse } from './lib/util.js'
import { transformColor, grayscale } from './lib/color-matrix.js'
import girl from './assets/girl1.jpg'

export default function drawPixel3() {
  const canvas = document.getElementById('paper3')
  const context = canvas.getContext('2d')

  ;(async function () {
    const img = await loadImage(girl)
    //const imageData = getImageData(img)
    const { width, height } = img
    canvas.width = width
    canvas.height = height
    context.drawImage(img, 0, 0)
    const imageData = context.getImageData(0, 0, width, height)

    canvas.width = imageData.width
    canvas.height = imageData.height
    context.putImageData(imageData, 0, 0)

    let percent = 0
    function update() {
      traverse(imageData, ({ r, g, b, a }) => {
        return transformColor([r, g, b, a], grayscale(percent))
      })
      context.putImageData(imageData, 0, 0)
      percent += 0.1
      if (percent <= 1) requestAnimationFrame(update)
    }
    update()
  })()
}
