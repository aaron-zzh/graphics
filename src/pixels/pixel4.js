import { loadImage, getImageData, traverse, getPixel } from './lib/util.js'
import { transformColor, brightness, saturate } from './lib/color-matrix.js'
import girl from './assets/girl1.jpg'
import light from './assets/sunlight.png'

export default function drawPixel4() {
  const canvas = document.getElementById('paper4')
  const context = canvas.getContext('2d')
  ;(async function () {
    const img = await loadImage(girl)
    const sunlight = await loadImage(light)
    const { width, height } = img

    // const imageData = getImageData(img);
    // const texture = getImageData(sunlight);
    canvas.width = width
    canvas.height = height
    context.drawImage(img, 0, 0)
    const imageData = context.getImageData(0, 0, width, height)
    canvas.width = width
    canvas.height = height
    context.drawImage(sunlight, 0, 0)
    const texture = context.getImageData(0, 0, width, height)

    traverse(imageData, ({ r, g, b, a, index }) => {
      const texColor = getPixel(texture, index)
      return transformColor(
        [r, g, b, a],
        brightness(1 + 0.7 * texColor[3]),
        saturate(2 - texColor[3])
      )
    })

    // 边缘模糊
    // traverse(imageData, ({ r, g, b, a, x, y }) => {
    //   const d = Math.hypot(x - 0.5, y - 0.5)
    //   a *= 1.0 - 2 * d
    //   return [r, g, b, a]
    // })

    canvas.width = imageData.width
    canvas.height = imageData.height
    context.putImageData(imageData, 0, 0)
  })()
}
