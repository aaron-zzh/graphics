import { loadImage, getImageData, traverse, gaussianBlur } from './lib/util.js'
import { transformColor, grayscale, saturate, brightness, contrast } from './lib/color-matrix.js'
import girl from './assets/girl1.jpg'

//channel 滤镜函数可以过滤或增强某个颜色通道
function channel({ r = 1, g = 1, b = 1 }) {
  return [r, 0, 0, 0, 0, 0, g, 0, 0, 0, 0, 0, b, 0, 0, 0, 0, 0, 1, 0]
}

export default function drawPixel2() {
  const canvas = document.getElementById('paper1')
  const context = canvas.getContext('2d')

  ;(async function () {
    const img = await loadImage(girl)
    // const imageData = getImageData(img) // 使用OffscreenCanvas渲染
    const { width, height } = img
    canvas.width = width
    canvas.height = height
    context.drawImage(img, 0, 0)
    const imageData = context.getImageData(0, 0, width, height)

    // 对每个像素进行处理
    traverse(imageData, ({ r, g, b, a }) => {
      //return transformColor([r, g, b, a], channel({ r: 1.2 }), brightness(1.2), saturate(1.2))
      return transformColor(
        [r, g, b, a],
        grayscale(0.5),
        saturate(1.2),
        contrast(1.1),
        brightness(1.2)
      )
    })

    // 高斯模糊
    gaussianBlur(imageData.data, width, height)

    canvas.width = imageData.width
    canvas.height = imageData.height
    context.putImageData(imageData, 0, 0)
  })()
}
