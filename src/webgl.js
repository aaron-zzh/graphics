// 创建着色器方法，输入参数：渲染上下文，着色器类型，数据源
function createShader(gl, type, source) {
  var shader = gl.createShader(type) // 创建着色器对象
  gl.shaderSource(shader, source) // 提供数据源
  gl.compileShader(shader) // 编译 -> 生成着色器
  var success = gl.getShaderParameter(shader, gl.COMPILE_STATUS)
  if (success) {
    return shader
  }

  console.log(gl.getShaderInfoLog(shader))
  gl.deleteShader(shader)
}

function createProgram(gl, vertexShader, fragmentShader) {
  var program = gl.createProgram()
  gl.attachShader(program, vertexShader)
  gl.attachShader(program, fragmentShader)
  gl.linkProgram(program)
  var success = gl.getProgramParameter(program, gl.LINK_STATUS)
  if (success) {
    return program
  }

  console.log(gl.getProgramInfoLog(program))
  gl.deleteProgram(program)
}

export default function webGLDraw() {
  const canvas = document.querySelector('.canvas_webgl')
  const gl = canvas.getContext('webgl')
  if (!gl) {
    // 你不能使用WebGL！
    return
  }
  // WebGL 会根据顶点和绘图模式指定的图元，计算出需要着色的像素点，然后对它们执行片元着色器程序。
  //顶点着色器（Vertex Shader）
  const vertex = ` 
    // 一个属性值，将会从缓冲中获取数据
    attribute vec2 position; 
    varying vec3 color;
    void main() {
      gl_PointSize = 1.0;
      color = vec3(0.5 + position * 0.5, 0.0);
      // gl_Position 是一个顶点着色器主要设置的变量
      gl_Position = vec4(position * 0.5, 1.0, 1.0);
    }
    `
  // 片元着色器（Fragment Shader）
  const fragment = `
    // 片断着色器没有默认精度，所以我们需要设置一个精度
    // mediump是一个不错的默认值，代表“medium precision”（中等精度）
    precision mediump float;
    varying vec3 color;
    void main() {
        gl_FragColor = vec4(color, 1.0);
    }
    `
  const vertexShader = createShader(gl, gl.VERTEX_SHADER, vertex)
  const fragmentShader = createShader(gl, gl.FRAGMENT_SHADER, fragment)
  const program = createProgram(gl, vertexShader, fragmentShader)

  // 定义这个三角形的三个顶点
  const points = new Float32Array([-1, -1, 0, 1, 1, -1])

  const bufferId = gl.createBuffer() //属性值从缓冲中获取数据，所以我们创建一个缓冲
  gl.bindBuffer(gl.ARRAY_BUFFER, bufferId) // 绑定一个数据源到绑定点
  gl.bufferData(gl.ARRAY_BUFFER, points, gl.STATIC_DRAW) // 将定义好的数据写入 WebGL 的缓冲区 gl.STATIC_DRAW提示WebGL我们不会经常改变这些数据。
  // 寻找属性值位置（和全局属性位置）应该在初始化的时候完成，而不是在渲染循环中。
  const vPosition = gl.getAttribLocation(program, 'position') //获取顶点着色器中的position变量的地址

  // 使用webglUtils库 setup GLSL program
  // var program = webglUtils.createProgramFromScripts(gl, ['vertex-shader-2d', 'fragment-shader-2d'])
  // gl.useProgram(program)
  // -----------------------
  // 在此之上的代码是 初始化代码。这些代码在页面加载时只会运行一次。
  // 接下来的代码是渲染代码，而这些代码将在我们每次要渲染或者绘制时执行。

  // 告诉WebGL怎么从我们之前准备的缓冲中获取数据给着色器中的属性。
  // 首先我们需要启用对应属性，激活这个变量
  gl.enableVertexAttribArray(vPosition)
  // 然后指定从缓冲中读取数据的方式
  gl.bindBuffer(gl.ARRAY_BUFFER, bufferId) // 将绑定点绑定到缓冲数据
  // 告诉属性怎么从positionBuffer中读取数据 (ARRAY_BUFFER)
  const size = 2 // 每次迭代运行提取两个单位数据
  const type = gl.FLOAT // 每个单位的数据类型是32位浮点型
  const normalize = false // 不需要归一化数据
  const stride = 0 // 0 = 移动单位数量 * 每个单位占用内存（sizeof(type)）
  // 每次迭代运行运动多少内存到下一个数据开始点
  var offset = 0 // 从缓冲起始位置开始读取
  gl.vertexAttribPointer(vPosition, size, type, normalize, stride, offset)

  gl.useProgram(program) // 告诉它用我们之前写好的着色程序（一个着色器对）
  gl.clearColor(0, 0, 0, 0) // 让画布变透明
  gl.clear(gl.COLOR_BUFFER_BIT) // 将当前画布的内容清除

  var primitiveType = gl.TRIANGLES //表示以三角形为图元绘制
  var offset = 0 // 顶点偏移量
  var count = 3 // 顶点数量
  gl.drawArrays(primitiveType, offset, count) //WebGL 就会将对应的 buffer 数组传给顶点着色器，并且开始绘制
}
