import colorRGB from './color-hints/rgb.js'
import colorHSL from './color-hints/hsl.js'
import colorHslProblem from './color-hints/hsl_problem.js'
import colorLAB from './color-hints/lab.js'
import colorCubehelix from './color-hints/cubehelix.js'
import drawWebGLGrids from './grids.js'
import drawWebGLMandel from './mandelbrot'
import drawMaze from './maze'
import drawRandom from './random'
import drawRandom2 from './random2'
import drawPixel1 from './pixels/pixel1'
import drawPixel2 from './pixels/pixel2'
import drawPixel3 from './pixels/pixel3'
import drawPixel34 from './pixels/pixel4'
import combine from './pixels/webgl_combine'
import particle from './pixels/webgl_particle'
import texture from './pixels/webgl_texture'
import draw3dCube from './3d_cube'

import './css/color.css'
;(async function () {
  particle()
  colorRGB()
  colorHSL()
  colorHslProblem()
  colorLAB()
  colorCubehelix()
  drawWebGLGrids()
  drawWebGLMandel()
  drawMaze()
  drawRandom()
  drawRandom2()
  drawPixel1()
  drawPixel2()
  drawPixel3()
  drawPixel34()
  combine()
  texture()
  draw3dCube()
  drawPlane()
})()
