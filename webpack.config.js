const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const webpack = require('webpack') // 访问内置的插件

module.exports = {
  mode: 'development',
  entry: {
    app: './src/index.js',
    print: './src/print.js',
    color_app: './src/color.js',
    git_3d: './src/3d_git.js'
  },
  devtool: 'inline-source-map', // 不可用于生产环境
  plugins: [
    new webpack.ProgressPlugin(),
    new HtmlWebpackPlugin({
      template: './src/index.html',
      chunks: ['app', 'print'] //每个html只引入对应的js和css
    }),
    new HtmlWebpackPlugin({
      template: './src/sprite3d.html',
      chunks: ['git_3d']
    }),
    new HtmlWebpackPlugin({
      filename: 'color.html', //打包后的文件名
      // minify: {
      //   //对html文件进行压缩
      //   removeAttributeQuotes: true, //去掉属性的双引号
      //   removeComments: true, //去掉注释
      //   collapseWhitespace: true //去掉空白
      // },
      chunks: ['color_app'], //每个html只引入对应的js和css
      inject: true,
      hash: true, //避免缓存js。
      template: './src/color.html' //打包html模版的路径和文件名称
    })
  ],
  output: {
    filename: '[name].bundle.js', //'[name].[contenthash].bundle.js',
    path: path.resolve(__dirname, 'dist')
  },
  devServer: {
    contentBase: './dist', //tell the dev server where to look for files
    hot: true
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            plugins: ['@babel/plugin-transform-runtime']
          }
        }
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.(png|jpe?g|gif)$/i,
        use: [
          {
            loader: 'file-loader'
          }
        ]
      }
    ]
  }
}
